<?php

/* AcmeParserBundle:default:index.html.twig */
class __TwigTemplate_3b0d7dc80f71381b07f8f53c414f49cbfc64f23ea6bafd3ef7681081811e271d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "price"), "html", null, true);
        echo "</p>
    <p>
    ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "photo"));
        foreach ($context['_seq'] as $context["_key"] => $context["ph"]) {
            // line 8
            echo "        <img width=\"200\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "ph"), "html", null, true);
            echo "\">
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ph'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "    </p>
";
    }

    public function getTemplateName()
    {
        return "AcmeParserBundle:default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 10,  45 => 8,  41 => 7,  36 => 5,  31 => 4,  28 => 3,);
    }
}
