<?php
namespace Acme\ParserBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;

Class Parser
{
    private $crawler;

    function __construct($url)
    {
        $html = file_get_contents($url);
        $this->crawler = new Crawler($html);
    }

    /**
     * ��������� ������������ ������
     * */
    function getTranslate($query) {
        $str_encode = urlencode($query);
        $url = "http://translate.google.ru/translate_a/t?client=x&text={$str_encode}&hl=auto&sl=auto&tl=ru&ie=UTF-8&oe=UTF-8";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);

        return $result->{'sentences'}[0]->{'trans'};
    }

    /**
     * ��������� ����� ������
     * */
    function getName()
    {
        return $this->crawler->filter('h3.tb-main-title')->text();
    }

    /**
     * ��������� �������� ������
     * */
    function getDescription()
    {
        $array_description = array();

        foreach($this->crawler->filter('div#attributes > ul.attributes-list li') as $elem)
            $array_description[] = trim($elem->textContent."\n");

        return $array_description;
    }

    /**
     * ��������� ���� ������ � �����
     * */
    function getPrice()
    {
        return $this->crawler->filter('div.tb-property-cont > strong > em')->eq(1)->text();
    }

    /**
     * ��������� ������ ���������� ������
     * */
    function getPhoto()
    {
        $array_photo = array();

        for ($i = 0; $i < count($this->crawler->filter('div.tb-gallery > ul#J_UlThumb li img')); $i++)
        {
            $array_photo[] = preg_replace("/50x50/", "400x400", $this->crawler->filter('div.tb-gallery > ul#J_UlThumb li img')->eq($i)->attr('data-src'));
        }
        return $array_photo;
    }

    /**
     * ��������� ��������� �������� ������
     * */
    function getSizes()
    {
        $array_available_sizes = array();

        foreach($this->crawler->filter('div.tb-skin > dl.tb-prop')->eq(0)->filter('dd > ul > li > a > span') as $elem)
        {
            $array_available_sizes[] = $elem->textContent;
        }
        return $array_available_sizes;
    }

    /**
     * ��������� ��������� ��������� ������
     * */
    function getColors()
    {
        $array_available_colors = array();

        foreach($this->crawler->filter('div.tb-skin > dl.tb-prop')->eq(1)->filter('dd > ul > li') as $elem)
        {
            $array_available_colors[] = trim($elem->textContent);
        }
        return $array_available_colors;
    }

    /**
     * �������������� ������� � ������ ��� ���������� � ����
     * */
    function implodeArray($array)
    {
        return implode(", ", $array);
    }

    /**
     * ������� ������ ���������
     * */
    function parseCategory()
    {
        $array_link = array();

        for ($i = 0; $i < count($this->crawler->filter("div.tshop-pbsm-shop-item-cates a")); $i++)
        {
            if(preg_match("/http:\/\//i", $this->crawler->filter("div.tshop-pbsm-shop-item-cates a")->eq($i)->attr('href')))
                $array_link[] = $this->crawler->filter("div.tshop-pbsm-shop-item-cates a")->eq($i)->attr('href');
        }
        return $array_link;
    }

    /**
     * ������� ������ �������
     * */
    function parseProduct()
    {
        $array_link = array();

        for ($i = 0; $i < count($this->crawler->filter("div#J_ShopSearchResult > div.skin-box-bd .photo a")); $i++)
        {
            $array_link[] = $this->crawler->filter("div#J_ShopSearchResult > div.skin-box-bd .photo a")->eq($i)->attr('href');
        }
        return $array_link;
    }

    /**
     * ������� ���������� �� �������� �������
     * */
    function parsePaginator()
    {
        $array_link = array();

        for ($i = 0; $i < count($this->crawler->filter("div.main-wrap  div.pagination a[class=J_SearchAsync]")); $i++)
        {
            $array_link[] = $this->crawler->filter("div.main-wrap  div.pagination a[class=J_SearchAsync]")->eq($i)->attr('href');
        }
        return $array_link;
    }
}