<?php

namespace Acme\ParserBundle\Controller;

use Acme\ParserBundle\Parser\Parser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\ParserBundle\Entity\Product;
use Acme\ParserBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;

class ParserController extends Controller
{
    /**
     * ������� ��������� �������� � �������
     * */
    public function indexAction()
    {
        $parser = new Parser("http://item.taobao.com/item.htm?id=37250280054&ali_trackid=2:mm_29954000_0_0:1400084466_6k1_1106514256&clk1=5bece6d3cda68d72c3a55da9554c2244#");

        $product = new Product();

        $product->setName( $parser->getTranslate($parser->getName()) );
        $product->setPrice( $parser->getPrice() );
        $product->setDescription( $parser->getTranslate($parser->implodeArray($parser->getDescription())) );
        $product->setPhoto( $parser->implodeArray($parser->getPhoto()) );
        $product->setSizes( $parser->getTranslate($parser->implodeArray($parser->getSizes())) );
        $product->setColors( $parser->getTranslate($parser->implodeArray($parser->getColors())) );

        $this->addToDataBase($product);

        return $this->render('AcmeParserBundle:default:index.html.twig', array(
            'title' => $parser->getTranslate($parser->getName()),
            'price' => $parser->getPrice(),
            'photo' => $parser->getPhoto()
        ));
    }

    /**
     * ������� ������ �� ������ ���������
     * */
    public function parseCategoryAction()
    {
        $parser = new Parser("http://miancun.taobao.com/category-512430307.htm?search=y&catName=HM%26amp%3BASOS%CF%B5%C1%D0%CD%AC%BF%EE");

        $array_link = $parser->parseCategory();

        foreach($array_link as $link)
        {
            $category = new Category();
            $category->setLink($link);

            $this->addToDataBase($category);
        }

        return $this->render('AcmeParserBundle:parseCategory:index.html.twig', array(
            'links' => $array_link
        ));
    }

    /**
     * ������� ������� �� ������ ������ �� ���������
     * */
    public function parsePageAction()
    {
        set_time_limit(0);

        // �������� ������ ������ ���������
        $links = $this->getDoctrine()
            ->getManager()
            ->getRepository('AcmeParserBundle:Category')
            ->findAllLinks();

        foreach ($links as $link)
        {
            $this->parsePag($link);
        }


//        $src = "http://miancun.taobao.com/category-934784476.htm?search=y&parentCatId=228543531&parentCatName=2014%D0%C2%BF%EE&catName=6%D4%C223%C8%D5%D6%DC%D2%BB%D0%C2%C6%B7#bd";
//        $this->parsePag($src);


        // ��������� �� ������ ������
//        foreach ($links as $link)
//        {
//            $parser = new Parser($link['link']);
//
//            $array_links_pagination = array_merge( (array)$link['link'], $parser->parsePaginator() );
//
//            foreach($array_links_pagination as $link_pagination)
//            {
//                // ������� �������� � ��������
//                $parse_pag = new Parser($link_pagination);
//
//                $array_link = $parse_pag->parseProduct();
//                // ���������� ���������� � ������ � ��
//                foreach ($array_link as $lnk)
//                {
//                    $parse = new Parser($lnk);
//
//                    $product = new Product();
//
//                    $product->setName( $parse->getTranslate($parse->getName()) );
//                    $product->setPrice( $parse->getPrice() );
//                    $product->setDescription( $parse->getTranslate($parse->implodeArray($parse->getDescription())) );
//                    $product->setPhoto( $parse->implodeArray($parse->getPhoto()) );
//                    $product->setSizes( $parse->getTranslate($parse->implodeArray($parse->getSizes())) );
//                    $product->setColors( $parse->getTranslate($parse->implodeArray($parse->getColors())) );
//
//                    $this->addToDataBase($product);
//                }
//            }
//        }

        return $this->render('AcmeParserBundle:parsePage:index.html.twig', array(
            'message' => 'done'
        ));
    }

    /**
     * ������� ���� ������� � ��������� ����� ���������
     * */
    public  function parsePag($pagination)
    {
        // ������ ������ ����������
        static $array_links_p = array();
        // ������ �� ����������� �������� ����������
        static $array_links_parsed = array();

        $parser = new Parser($pagination);

        $pagin = $parser->parsePaginator();

        foreach($pagin as $link)
        {
            if (!in_array($link, $array_links_p))
                $array_links_p[] = $link;
            else
                continue;
        }

        // ��������� �� ������ ������ � ������ ������
        for ($i = 0; $i < count($array_links_p); $i++)
        {
            if (!in_array($array_links_p[$i], $array_links_parsed))
                $array_links_parsed[] = $array_links_p[$i];
            else
                continue;

            // ������� �������� � ��������
            $parse_pag = new Parser($array_links_p[$i]);

            $array_link = $parse_pag->parseProduct();
            // ���������� ���������� � ������ � ��
            foreach ($array_link as $lnk)
            {
                $parse = new Parser($lnk);

                $product = new Product();

                $product->setName( $parse->getTranslate($parse->getName()) );
                $product->setPrice( $parse->getPrice() );
                $product->setDescription( $parse->getTranslate($parse->implodeArray($parse->getDescription())) );
                $product->setPhoto( $parse->implodeArray($parse->getPhoto()) );
                $product->setSizes( $parse->getTranslate($parse->implodeArray($parse->getSizes())) );
                $product->setColors( $parse->getTranslate($parse->implodeArray($parse->getColors())) );

                $this->addToDataBase($product);
            }

            echo count($array_links_parsed);

            if($i == (count($array_links_p) - 1))
                $this->parsePag($array_links_p[$i]);
        }
    }

    /**
     * ����� ����� ������ ������ �� ���������
     * */
    public function showCategoryAction()
    {
        $repo = $this->getDoctrine()->getRepository('AcmeParserBundle:Category');
        $products = $repo->findAll();

        return $this->render('AcmeParserBundle:showCategory:index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * ����� ���������� �� ����� ������
     * */
    public function showProductAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AcmeParserBundle:Product');
        $products = $repo->findBy(
            array('id' => $id)
        );

        return $this->render('AcmeParserBundle:showProduct:index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * ���������� ������ � ��
     * */
    private function addToDataBase($obj)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($obj);
        $em->flush();
    }
}