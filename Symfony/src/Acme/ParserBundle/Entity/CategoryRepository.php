<?php

namespace Acme\ParserBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function findAllLinks()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT c.link FROM AcmeParserBundle:Category c')
            ->getResult();
    }


}